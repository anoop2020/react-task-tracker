import './App.css';
import Header from './components/Header';
import Tasks from './components/Tasks';
import {useState} from 'react';


function App() {
  const [tasks,setTasks] = useState([
    {
        id:1,
        text:'Doctors Appointment',
        day: 'Feb5th at 2:30 PM',
        reminder:true,
    },
    {
        id:2,
        text:'Doctor2s Appointment',
        day: 'Feb6th at 2:30 PM',
        reminder:true,
    },
    {
        id:3,
        text:'Doctor3s Appointment',
        day: 'Feb7th at 2:30 PM',
        reminder:false,
    },
])

//Delete Task

const deleteTask = (id) =>{
  setTasks(tasks.filter((tasks) => tasks.id !==id))
}
  return (
    <div className="container">
      <Header />
      {tasks.length >0 ? (
      <Tasks tasks={tasks} onDelete={deleteTask}/>)
      : ('No Tasks to Show')}
    </div>
  );
}

export default App;
